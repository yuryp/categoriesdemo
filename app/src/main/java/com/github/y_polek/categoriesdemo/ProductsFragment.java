package com.github.y_polek.categoriesdemo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.y_polek.categoriesdemo.model.Product;
import com.github.y_polek.categoriesdemo.utils.DividerItemDecoration;
import com.github.y_polek.categoriesdemo.utils.Parcelables;

public class ProductsFragment extends Fragment {

    private static final String KEY_PRODUCTS = "key_products";

    private ProductsAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ProductsAdapter(readProductsArg());
    }

    private Product[] readProductsArg() {
        Product[] products = null;
        Bundle args = getArguments();
        if (args != null) {
            products = Parcelables.toArrayOfType(Product.class, args.getParcelableArray(KEY_PRODUCTS));
        }
        if (products == null) {
            throw new IllegalArgumentException("Products list must be provided as an argument");
        }
        return products;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(requireContext()));
        recyclerView.setAdapter(adapter);

        return view;
    }

    public static ProductsFragment newInstance(Product[] products) {
        Bundle args = new Bundle();
        args.putParcelableArray(KEY_PRODUCTS, products);
        ProductsFragment fragment = new ProductsFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
