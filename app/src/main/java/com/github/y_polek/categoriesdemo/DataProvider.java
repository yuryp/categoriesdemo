package com.github.y_polek.categoriesdemo;

import com.github.y_polek.categoriesdemo.model.Category;
import com.github.y_polek.categoriesdemo.model.Product;

public class DataProvider {

    public static Category[] generateFakeData() {
        return new Category[] {
                new Category("Cleaning",
                        new Category("Chemicals",
                                new Category("Laundry Supplies",
                                        new Product("Softener", 5.0),
                                        new Product("Detergent", 3.5),
                                        new Product("Detergent Liquid", 2.5),
                                        new Product("Detergent Bottle", 4.5)),
                                new Category("Cleaners",
                                        new Product("Degreaser", 1.0),
                                        new Product("Cleaner", 0.99))),
                        new Category("Equipment",
                                new Product("Bags", 7.0),
                                new Product("Vacuum", 150.0))),
                new Category("Computers",
                        new Category("Nettops",
                                new Product("Asus Nettop N123", 300.0),
                                new Product("Dell Nettop N567", 500.0)),
                        new Category("Laptops",
                                new Product("Asus Laptop L123", 300.0),
                                new Product("Dell Laptop L567", 500.0)),
                        new Category("PCs",
                                new Product("Asus PC P123", 300.0),
                                new Product("Dell PC P567", 500.0)))
        };
    }
}
