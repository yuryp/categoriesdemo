package com.github.y_polek.categoriesdemo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.y_polek.categoriesdemo.model.Category;
import com.github.y_polek.categoriesdemo.utils.OnItemClickListener;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private final Category[] categories;
    private final OnItemClickListener listener;

    public CategoriesAdapter(@NonNull Category[] categories, OnItemClickListener listener) {
        this.categories = categories;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        return new ViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == RecyclerView.NO_POSITION) return;
        holder.bind(getItemAtPosition(position));
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    public Category getItemAtPosition(int position) {
        return categories[position];
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        ViewHolder(@NonNull final View itemView, final OnItemClickListener listener) {
            super(itemView);
            textView = itemView.findViewById(R.id.text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClicked(getAdapterPosition());
                    }
                }
            });
        }

        void bind(Category category) {
            textView.setText(category.name);
        }
    }
}
