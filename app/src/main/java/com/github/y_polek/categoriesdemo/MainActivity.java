package com.github.y_polek.categoriesdemo;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.github.y_polek.categoriesdemo.model.Category;
import com.github.y_polek.categoriesdemo.model.Product;
import com.github.y_polek.categoriesdemo.utils.OnCategorySelectedListener;
import com.github.y_polek.categoriesdemo.utils.Parcelables;

public class MainActivity extends AppCompatActivity implements OnCategorySelectedListener {

    private static final String KEY_CATEGORIES = "key_categories";
    private static final String FRAGMENT_TAG_CATEGORIES = "fragment_tag_categories";
    private Category[] rootCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            rootCategories = DataProvider.generateFakeData();
            showCategoriesFragment(rootCategories, true);
        } else {
            rootCategories = Parcelables.toArrayOfType(Category.class, savedInstanceState.getParcelableArray(KEY_CATEGORIES));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArray(KEY_CATEGORIES, rootCategories);
    }

    @Override
    public void onCategorySelected(Category category) {
        if (category.categories.length > 0) {
            showCategoriesFragment(category.categories, false);
        } else {
            showProductsFragment(category.products);
        }
    }

    private void showCategoriesFragment(Category[] categories, boolean isRoot) {
        CategoriesFragment fragment = CategoriesFragment.newInstance(categories);
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        if (!isRoot) {
            transaction.setCustomAnimations(
                    R.anim.enter_from_right, R.anim.exit_to_left,
                    R.anim.enter_from_left, R.anim.exit_to_right);
        }
        transaction.replace(R.id.fragment_container, fragment, FRAGMENT_TAG_CATEGORIES);
        if (!isRoot) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private void showProductsFragment(Product[] products) {
        ProductsFragment fragment = ProductsFragment.newInstance(products);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.enter_from_right, R.anim.exit_to_left,
                        R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.fragment_container, fragment, FRAGMENT_TAG_CATEGORIES)
                .addToBackStack(null)
                .commit();
    }
}
