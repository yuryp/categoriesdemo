package com.github.y_polek.categoriesdemo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.y_polek.categoriesdemo.model.Category;
import com.github.y_polek.categoriesdemo.utils.DividerItemDecoration;
import com.github.y_polek.categoriesdemo.utils.OnCategorySelectedListener;
import com.github.y_polek.categoriesdemo.utils.OnItemClickListener;
import com.github.y_polek.categoriesdemo.utils.Parcelables;

public class CategoriesFragment extends Fragment implements OnItemClickListener {

    private static final String KEY_CATEGORIES = "key_categories";

    private CategoriesAdapter adapter;
    private OnCategorySelectedListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new CategoriesAdapter(readCategoriesArg(), this);
        listener = readListener();
    }

    private Category[] readCategoriesArg() {
        Category[] categories = null;
        Bundle args = getArguments();
        if (args != null) {
            categories = Parcelables.toArrayOfType(Category.class, args.getParcelableArray(KEY_CATEGORIES));
        }
        if (categories == null) {
            throw new IllegalArgumentException("Categories list must be provided as an argument");
        }
        return categories;
    }

    @Nullable
    private OnCategorySelectedListener readListener() {
        Activity activity = getActivity();
        if (activity instanceof OnCategorySelectedListener) {
            return (OnCategorySelectedListener) activity;
        }
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(requireContext()));
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onItemClicked(int position) {
        if (listener != null) listener.onCategorySelected(adapter.getItemAtPosition(position));
    }

    public static CategoriesFragment newInstance(Category[] categories) {
        Bundle args = new Bundle();
        args.putParcelableArray(KEY_CATEGORIES, categories);
        CategoriesFragment fragment = new CategoriesFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
