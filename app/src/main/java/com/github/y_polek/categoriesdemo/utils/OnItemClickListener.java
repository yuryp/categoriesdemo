package com.github.y_polek.categoriesdemo.utils;

public interface OnItemClickListener {

    void onItemClicked(int position);
}
