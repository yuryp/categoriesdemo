package com.github.y_polek.categoriesdemo.utils;

import com.github.y_polek.categoriesdemo.model.Category;

public interface OnCategorySelectedListener {

    void onCategorySelected(Category category);
}
