package com.github.y_polek.categoriesdemo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.y_polek.categoriesdemo.model.Product;

import java.util.Locale;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private final Product[] products;

    public ProductsAdapter(@NonNull Product[] products) {
        this.products = products;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == RecyclerView.NO_POSITION) return;
        holder.bind(getItemAtPosition(position));
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public Product getItemAtPosition(int position) {
        return products[position];
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView nameText;
        private TextView priceText;

        ViewHolder(@NonNull final View itemView) {
            super(itemView);
            nameText = itemView.findViewById(R.id.name_text);
            priceText = itemView.findViewById(R.id.price_text);
        }

        void bind(Product product) {
            nameText.setText(product.name);
            priceText.setText(String.format(Locale.ROOT, "$%.2f", product.price));
        }
    }
}
