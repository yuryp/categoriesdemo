package com.github.y_polek.categoriesdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;
import java.util.Objects;

public class Category implements Parcelable {

    public final String name;
    public final Category[] categories;
    public final Product[] products;

    public Category(String name, Category... categories) {
        this.name = name;
        this.categories = categories;
        this.products = new Product[0];
    }

    public Category(String name, Product... products) {
        this.name = name;
        this.categories = new Category[0];
        this.products = products;
    }

    protected Category(Parcel in) {
        name = in.readString();
        categories = in.createTypedArray(Category.CREATOR);
        products = in.createTypedArray(Product.CREATOR);
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(name, category.name) &&
                Arrays.equals(categories, category.categories) &&
                Arrays.equals(products, category.products);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(categories);
        result = 31 * result + Arrays.hashCode(products);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", categories=" + Arrays.toString(categories) +
                ", products=" + Arrays.toString(products) +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeTypedArray(categories, i);
        parcel.writeTypedArray(products, i);
    }
}
